package testcases;


import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.beust.jcommander.Parameter;
import com.yalla.selenium.api.base.SeleniumBase;


public class Annotation extends SeleniumBase {

	@Parameters({"url","username","password"})
	@BeforeMethod
	public void Beforemethod() 
	{
		startApp("ie", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		
	}



	@AfterMethod
	public void AfterMethod() {
		close();
	}



	@Beforeclass
	public void Beforeclass()
	{
	}


	@AfterClass
	public void Afterclass()
	{
	}



}





















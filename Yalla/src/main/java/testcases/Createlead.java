package testcases;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;

import com.yalla.selenium.api.base.SeleniumBase;


	
	
	    
	    public class Createlead<object> extends SeleniumBase {

	    
        @Test
		public void createLead()
		{
			startApp("chrome", "http://leaftaps.com/opentaps");
			WebElement eleUserName = locateElement("id", "username");
			clearAndType(eleUserName, "DemoSalesManager");
			WebElement elePassword = locateElement("id", "password");
			clearAndType(elePassword, "crmsfa");
			WebElement eleLogin = locateElement("class", "decorativeSubmit");
			click(eleLogin);
			WebElement eleLink = locateElement("link","CRM/SFA");
			click(eleLink);
			
			WebElement eleLeads = locateElement("xpath","//a[text()='Leads']");
			click(eleLeads);
			
			WebElement eleCreate = locateElement("xpath","//a[text()='Create Lead']");
			click(eleCreate);
			
			//CompanyName,First Name and Last Name
			WebElement eleCompName = locateElement("id","createLeadForm_companyName");
			clearAndType(eleCompName,"Wipro");
			String expText = eleCompName.getText();
			WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
			clearAndType(eleFirstName,"Pabitha");
			WebElement eleLastName = locateElement("id","createLeadForm_lastName");
			clearAndType(eleLastName,"J");
			
			//DropDown
			WebElement eleSourceDropDown = locateElement("id","createLeadForm_dataSourceId");
			selectDropDownUsingIndex(eleSourceDropDown,1);
			
			WebElement eleSubButton = locateElement("xpath","//input[@name='submitButton']");
			click(eleSubButton);
			
			WebElement eleVerifyCompName = locateElement("id","viewLead_companyName_sp");
			//String actText = eleVerifyCompName.getText();
			
			
			verifyPartialText(eleVerifyCompName, expText);
	}
	}
	    
	    @DataProvider(name = "CreateData")
		public void fetchdata()
		{
			public Object[][] data =  new Object[2][3];
			data[0] = "TestLeaf";
			data[0][1] = "Pabitha";
			data[0[2]] = 'J';
					
			data[1][0]= "IL";
		    data[1][1] = "Pabitha";
		    data[1{2] = "J";
		    return data;
		    
		    };




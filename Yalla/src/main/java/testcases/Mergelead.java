package testcases;

import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.SeleniumBase;

public class Mergelead {
	
	
	public class Createlead extends SeleniumBase {
		
		

		@Test
		public void createLead() throws InterruptedException
		{
			startApp("chrome", "http://leaftaps.com/opentaps");
			WebElement eleUserName = locateElement("id", "username");
			clearAndType(eleUserName, "DemoSalesManager");
			WebElement elePassword = locateElement("id", "password");
			clearAndType(elePassword, "crmsfa");
			WebElement eleLogin = locateElement("class", "decorativeSubmit");
			click(eleLogin);
			WebElement eleLink = locateElement("xpath","//div[@id='label']/a");
			click(eleLink);
			
			WebElement eleMergeLead = locateElement("xpath","//a[text()='Merge Leads']");
			click(eleMergeLead);
			
			WebElement ele = locateElement("xpath","//table[@id='widget_ComboBox_partyIdFrom']/following-sibling::a");
			click(ele);
			
			switchToWindow(1);
			
			WebElement eleLeadID = locateElement("xpath","//input[@name='id']");
			clearAndType(eleLeadID,"10052");
			
			WebElement eleFindLead = locateElement("xpath","//button[text()='Find Leads']");
			click(eleFindLead);
			Thread.sleep(2000);
			
			WebElement eleFirstElement = locateElement("xpath","(//a[@class='linktext'])[1]");
			click(eleFirstElement);
			
			switchToWindow(0);
			
			WebElement eleToElement= locateElement("xpath","//table[@id='widget_ComboBox_partyIdTo']/following-sibling::a/img");
			click(eleToElement);
			
			switchToWindow(1);
			
			WebElement eleLeadID1 = locateElement("xpath","//input[@name='id']");
			clearAndType(eleLeadID1,"10038");
			
			WebElement eleToID = locateElement("xpath","//button[text()='Find Leads']");
			click(eleToID);
			
			Thread.sleep(2000);
			
			WebElement eleFirstToEle = locateElement("xpath","(//a[@class='linktext'])[1]");
			String elementText1 = getElementText(eleFirstToEle);
			takeSnap();
			click(eleFirstToEle);
			
			switchToWindow(0);
			
			WebElement eleMerge = locateElement("xpath","//a[@class='buttonDangerous']");
			click(eleMerge);
			
			acceptAlert();
			takeSnap();
			
			WebElement eleMergedEle = locateElement("id","viewLead_companyName_sp");
			String elementText2 = getElementText(eleMergedEle);
		
			if(elementText1.contains(elementText2))
				System.out.println("Merged Text is sucessfully updated");
			else
				System.out.println("Merged Text is not updated ");
			
			WebElement eleDuplicateLead = locateElement("link","Duplicate Lead");
			click(eleDuplicateLead);
		
			WebElement eleDupCompName = locateElement("id","createLeadForm_companyName");
			String compName = getElementText(eleDupCompName);
			
			WebElement eleCreateLeadButton = locateElement("xpath","//input[@name='submitButton']");
			click(eleCreateLeadButton);
			
			WebElement eleDupCmpName = locateElement("xpath","viewLead_companyName_sp");
			getElementText(eleDupCmpName);
		}
	}
			
			

}

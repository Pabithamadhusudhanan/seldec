package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.SeleniumBase;

public class Editlead extends SeleniumBase{


	@Test /*(invocationCount = 2, invocationTimeOut = 1000)*/
	public void editLead() throws InterruptedException
	{
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleLink = locateElement("xpath","//div[@id='label']/a");
		click(eleLink);

		WebElement eleLeads = locateElement("xpath","//a[text()='Leads']");
		click(eleLeads);

		WebElement eleCreate = locateElement("xpath","//a[text()='Create Lead']");
		click(eleCreate);

		//CompanyName,First Name and Last Name
		WebElement eleCompName = locateElement("id","createLeadForm_companyName");
		clearAndType(eleCompName,"Wipro");
		String expText = eleCompName.getText();
		WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
		clearAndType(eleFirstName,"Pabitha");
		WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		clearAndType(eleLastName,"J");

		//DropDown
		WebElement eleSourceDropDown = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingIndex(eleSourceDropDown,1);

		WebElement eleSubButton = locateElement("xpath","//input[@name='submitButton']");
		click(eleSubButton);

		WebElement eleVerifyCompName = locateElement("id","viewLead_companyName_sp");
		//String actText = eleVerifyCompName.getText();


		verifyPartialText(eleVerifyCompName, expText);
	}

	public void startApp(String string, String string2) {
		// TODO Auto-generated method stub

	}
}





package com.yalla.selenium.api.base;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.yalla.selenium.api.design.Browser;
import com.yalla.selenium.api.design.Element;

public class SeleniumBase implements Browser, Element{

	public RemoteWebDriver driver;
	public WebDriverWait wait;
	
	public void click(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			System.out.println("The Element "+ele+" clicked");
		} catch (StaleElementReferenceException e) {
			System.err.println("The Element "+ele+" could not be clicked");
			throw new RuntimeException();
		}

	}

	public void append(WebElement ele, String data) {
		// TODO Auto-generated method stub

	}

	
	public void clear(WebElement ele) {
		// TODO Auto-generated method stub

	}

	
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			System.out.println("The Data :"+data+" entered Successfully");
		} catch (ElementNotInteractableException e) {
			System.err.println("The Element "+ele+" is not Interactable");
			throw new RuntimeException();
		}

	}

	
	public String getElementText(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public String getBackgroundColor(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public String getTypedText(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub

	}

	
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub

	}

	
	public void selectDropDownUsingValue(WebElement ele, String value) {
		// TODO Auto-generated method stub

	}

	
	public boolean verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	
	public boolean verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public boolean verifyDisappeared(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public boolean verifyEnabled(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public boolean verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public void startApp(String url) {
		// TODO Auto-generated method stub

	}

	
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver",
						"./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver",
						"./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			} else if(browser.equalsIgnoreCase("ie")) {
				System.setProperty("webdriver.ie.driver",
						"./drivers/IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			}
			driver.navigate().to(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.err.println("The Browser Could not be Launched. Hence Failed");
			throw new RuntimeException();
		}

	}

	
	public WebElement locateElement(String locatorType, String value) {
		try {
			switch(locatorType.toLowerCase()) {
			case "id": return driver.findElementById(value);
			case "name": return driver.findElementByName(value);
			case "class": return driver.findElementByClassName(value);
			case "link": return driver.findElementByLinkText(value);
			case "xpath": return driver.findElementByXPath(value);
			}
		} catch (NoSuchElementException e) {
			System.err.println("The Element with locator:"+locatorType+" Not Found with value: "+value);
			throw new RuntimeException();
		}
		return null;
	}

	
	public WebElement locateElement(String value) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public List<WebElement> locateElements(String type, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void switchToAlert() {
		// TODO Auto-generated method stub

	}

	
	public void acceptAlert() {
		// TODO Auto-generated method stub

	}

	
	public void dismissAlert() {
		// TODO Auto-generated method stub

	}

	
	public String getAlertText() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void typeAlert(String data) {
		// TODO Auto-generated method stub

	}

	
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub

	}

	
	public void switchToWindow(String title) {
		// TODO Auto-generated method stub

	}

	
	public void switchToFrame(int index) {
		// TODO Auto-generated method stub

	}

	
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub

	}

	
	public void switchToFrame(String idOrName) {
		// TODO Auto-generated method stub

	}

	
	public void defaultContent() {
		// TODO Auto-generated method stub

	}

	
	public boolean verifyUrl(String url) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public boolean verifyTitle(String title) {
		
		String title2 = driver.getTitle();
		// TODO Auto-generated method stub
		return false;
	}

	public int i=0;
	public void takeSnap() {
		
		File src = driver.getScreenshotAs(OutputType.FILE);	
		File des = new File("./snaps/snap"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
					
		// TODO Auto-generated method stub

	

	}
	
	public void close() {
		// TODO Auto-generated method stub

	}

	
	public void quit() {
		// TODO Auto-generated method stub

	}

}
